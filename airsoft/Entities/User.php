<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 22.05.2016
 * Time: 11:49
 */

namespace Airsoft\Entities;

use Airsoft\Repo\UserRepo\UserRepoInterface;

final class User
{
    protected $userRepo;
    protected $name;
    protected $email;
    protected $password;
    protected $confirmationCode;
    protected $apiToken;

    /**
     * User constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $confirmationCode
     * @param $userRepo
     */
    public function __construct($name, $email, $password, $confirmationCode, UserRepoInterface $userRepo)
    {
        $this->userRepo         = $userRepo;
        $this->name             = $name;
        $this->email            = $email;
        $this->password         = bcrypt($password);
        $this->confirmationCode = $confirmationCode;
        $this->apiToken         = str_random(60);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * @param string $confirmationCode
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * @return string
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }
}