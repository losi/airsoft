<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 22.05.2016
 * Time: 9:52
 */

namespace Airsoft\Repo;


use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('Airsoft\Repo\UserRepo\UserRepoInterface', 'Airsoft\Repo\UserRepo\UserEloquentRepo');
    }
}