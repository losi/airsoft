<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 16.05.2016
 * Time: 20:47
 */

namespace Airsoft\Repo\UserRepo;

use App\User;
use Airsoft\Entities\User as UserEntity;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserEloquentRepo implements UserRepoInterface
{
    protected $user;

    /**
     * UserEloquentRepo constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get current authenticated User
     * @return mixed
     */
    public function getAuthUser()
    {
        return Auth::user();
    }

    /**
     * Get User's coordinates by id
     *
     * @param integer $id
     *
     * @return User
     */
    public function getUserCoordinateById($id)
    {
        return $this->user->findOrFail($id)->coordinates;
    }

    /**
     * Create new User.
     *
     * @param UserEntity $user
     *
     * @return void
     * @throws QueryException
     */
    public function persistUser(UserEntity $user)
    {
        $user = $this->user->create([
            'name'              => $user->getName(),
            'email'             => $user->getEmail(),
            'password'          => $user->getPassword(),
            'confirmation_code' => $user->getConfirmationCode(),
            'api_token'         => $user->getApiToken(),
        ]);

        if ( ! $user) {
            throw QueryException::class;
        }
    }

    /**
     * Get User by confirmation code (email verification)
     *
     * @param string $confirmationCode
     *
     * @return User
     */
    public function findUserByConfirmationCode($confirmationCode)
    {
        return $this->user->where('confirmation_code', $confirmationCode)->firstOrFail();
    }

    /**
     * Get User by email
     *
     * @param string $email
     *
     * @return User
     */
    public function getByEmail($email)
    {
        return $this->user->where('email', $email)->firstOrFail();
    }

    /**
     * Change User's password and activate
     *
     * @param User $user
     * @param string $password
     */
    public function changePasswordAndActivate($user, $password)
    {
        $user->password       = Hash::make($password);
        $user->confirmed      = 1;
        $user->remember_token = null;
        $user->save();
    }

    /**
     * Activate user after he came from link in verification email.
     *
     * @param string $confirmationCode
     *
     * @return integer user id
     */
    public function confirmEmail($confirmationCode)
    {
        $user = $this->findUserByConfirmationCode($confirmationCode);

        $user->confirmed         = 1;
        $user->confirmation_code = null;
        $user->save();

        return $user->id;
    }
}