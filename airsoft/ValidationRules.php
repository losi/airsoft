<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 22.05.2016
 * Time: 10:15
 */

namespace Airsoft;


/**
 * Class ValidationRules
 * @package Airsoft
 */
class ValidationRules
{
    /**
     * All rules
     * @var array
     */
    private $rulesForEloquent  = array(
        'name' => 'required|max:50',
        'email' => 'required|email|max:50',
        'email_registration' => 'required|email|max:50|unique:users,email',
        'password' => 'required|min:6|confirmed',
        'password_login' => 'required|min:6',
    );

    /**
     * Get rules for Validator
     * @param array $fields to validate (strings) 
     * @return array
     */
    public function getRules($fields) {
        $rules = array();
        foreach ($fields as $field) {
            if (array_key_exists($field, $this->rulesForEloquent)) {
                $rules[$field] = $this->rulesForEloquent[$field];
            }
        }

        return $rules;
    }
}