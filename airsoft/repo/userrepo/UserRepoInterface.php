<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 16.05.2016
 * Time: 20:31
 */

namespace Airsoft\Repo\UserRepo;

use Airsoft\Entities\User;

interface UserRepoInterface
{
    /**
     * Create new user.
     * @param User $user
     * @return void
     */
    public function persistUser(User $user);

    public function findUserByConfirmationCode($confirmationCode);

    public function getAuthUser();

    public function getUserCoordinateById($id);

    public function getByEmail($email);

    public function changePasswordAndActivate($user, $password);

    public function confirmEmail($confirmationCode);
}