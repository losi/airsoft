<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
    use SoftDeletes;
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Determine if current user is admin.
     * @return bool
     */
    public function isAdmin()
    {
        $admin = Auth::user()->admin;

        return $admin ? true : false;
    }
}
