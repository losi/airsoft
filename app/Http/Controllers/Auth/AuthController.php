<?php

namespace App\Http\Controllers\Auth;

use Airsoft\Entities\User;
use Airsoft\Repo\UserRepo\UserRepoInterface;
use Airsoft\ValidationRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $userRepo;

    protected $validationRules;

    /**
     * Create a new authentication controller instance.
     *
     * @param UserRepoInterface $userRepo
     */
    public function __construct(UserRepoInterface $userRepo)
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->userRepo = $userRepo;
        $this->validationRules = new ValidationRules();
    }

//    /**
//     * Get a validator for an incoming registration request.
//     *
//     * @param  array $data
//     * @return \Illuminate\Contracts\Validation\Validator
//     */
//    protected function validator(array $data)
//    {
//        return Validator::make($data, [
//            'name' => 'required|max:50',
//            'email' => 'required|email|max:50|unique:users',
//            'password' => 'required|min:6|confirmed',
//        ]);
//    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function createUser(array $data)
    {
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
//        ]);
        $user = new User($data['name'], $data['email_registration'], $data['password'], $data['confirmationCode'], $this->userRepo);
        $this->userRepo->persistUser($user);
    }

    public function register(Request $request)
    {
        $fields = ['name', 'email_registration', 'password', 'password_confirmation',];
        $this->validate($request, $this->validationRules->getRules($fields));
        $request = $request->only($fields);

        $confirmationCode = str_random(60);
        // send verification email.
        Mail::send('email.verify', ['confirmationCode' => $confirmationCode], function ($message) use ($request) {
            $message->to($request['email_registration'], $request['name'])->subject('Verify your email address');
        });

        $request['confirmationCode'] = $confirmationCode;
        $this->createUser($request);

        return redirect('/')->with('message', 'Thanks for signing up! Please check your email.');
    }

    public function confirm($confirmationCode)
    {
        if (!$confirmationCode) {
            throw \InvalidArgumentException::class;
        }
        $userId = $this->userRepo->confirmEmail($confirmationCode);

        Auth::loginUsingId($userId);

        return redirect('/home')->with('message', 'You have successfully verified your account.');
    }

    public function login(Request $request)
    {
        $fields = ['email', 'password_login'];
        $this->validate($request, $this->validationRules->getRules($fields));
        $request = $request->only($fields);
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password_login'], 'confirmed' => 1])) {
            return redirect()->intended('/home');
        }

        return redirect()->guest('login')->withInput()->with('message', 'Wrong credentials');
    }
}
