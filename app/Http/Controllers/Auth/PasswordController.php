<?php

namespace App\Http\Controllers\Auth;

use Airsoft\Repo\UserRepo\UserRepoInterface;
use Airsoft\ValidationRules;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $validationRules;
    protected $userRepo;

    /**
     * Create a new password controller instance.
     *
     * @param UserRepoInterface $userRepo
     * @return mixed
     */
    public function __construct(UserRepoInterface $userRepo)
    {
        $this->middleware('guest');
        $this->validationRules = new ValidationRules();
        $this->userRepo = $userRepo;
    }

    /**
     * Change User's password, activate and authenticate.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reset(Request $request)
    {
        $fields = ['email', 'password', 'password_confirmation'];
        $this->validate($request, $this->validationRules->getRules($fields));

        $data = $request->only($fields);
        $user = $this->userRepo->getByEmail($data['email']);
        $this->userRepo->changePasswordAndActivate($user, $data['password']);
        Auth::login($user);

        return redirect('/');
    }
}
