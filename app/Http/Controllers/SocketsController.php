<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use \ZMQContext;

class SocketsController extends Controller
{
    public function index()
    {
        return view('sockets.test');
    }

    public function pusher()
    {
        return view('sockets.pusher');
    }

    public function store(Request $request)
    {
        $input   = $request->all();
        $context = new ZMQContext();
        $socket  = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
        $socket->connect('tcp://localhost:5555');
        $socket->send(json_encode($input));
    }
}
