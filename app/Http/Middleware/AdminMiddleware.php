<?php

namespace App\Http\Middleware;

use Closure;
use App\Admin;
use Illuminate\Support\Facades\Auth;

/**
 * Check if user is admin.
 * Class AdminMiddleware
 * @package App\Http\Middleware
 */
class AdminMiddleware
{
    protected $admin;

    /**
     * AdminMiddleware constructor.
     */
    public function __construct()
    {
        $this->admin = new Admin();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || !$this->admin->isAdmin()) {
            return redirect('/');
        }

        return $next($request);
    }
}
