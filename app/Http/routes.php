<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();
Route::get('register/verify/{confirmationCode}', [
    'as'   => 'confirmation_path',
    'uses' => 'Auth\AuthController@confirm',
]);

// home page
Route::get('/home', 'HomeController@index');

// test sockets
Route::get('/test-sockets', 'SocketsController@index')->middleware('admin');
Route::get('/test-pusher', 'SocketsController@pusher')->middleware('admin');
Route::post('/test-pusher', 'SocketsController@store')->middleware('admin');

// test api auth
Route::get('/test-api', 'ApiController@test')->middleware('auth:api');

// admin page
Route::get('/admin', function () {
    echo 'admin page';
})->middleware('admin');
