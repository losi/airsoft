<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('email', 50)->unique();
            $table->string('password', 60);
            $table->string('api_token', 60)->unique();
            $table->unsignedTinyInteger('team')->nullable();
            $table->unsignedTinyInteger('status')->nullable();
            $table->bigInteger('coordinates')->nullable();
            $table->rememberToken();
            $table->boolean('confirmed')->default(0);
            $table->string('confirmation_code', 60)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
