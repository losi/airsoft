/**
 * Created by losi on 02.06.2016.
 */

// var conn = new WebSocket('ws://localhost:8080');
// conn.onopen = function(e) {
//     console.log("Connection established!");
// };
//
// conn.onmessage = function(e) {
//     console.log(e.data);
// };
//
// conn.send('Hello World!');

$(document).ready(function () {
    var form = $('#sockets-test');

    var conn = new WebSocket(form.data('url') + ':' + form.data('port'));
    conn.onopen = function (e) {
        console.log("Connection established!");
    };

    conn.onmessage = function (e) {
        console.log(e.data);
        var response = '<li>' + e.data + '</li>';
        $('#socket-response').append(response);
    };

    form.submit(function (event) {
        event.preventDefault();
        var data = form.find('#data').val();
        console.log(data);
        conn.send(data);
    });
});