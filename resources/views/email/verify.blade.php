<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Verify Your Email Address</h2>
<div>
    Thanks for creating an account with the airsoft.app.
    Please follow the <a href="{{ URL::to('register/verify/' . $confirmationCode) }}">link</a> below to verify your
    email address.<br/>
</div>
</body>
</html>