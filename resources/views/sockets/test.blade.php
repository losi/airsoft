@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Type data to send</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="sockets-test" data-url="{{env('SOCKET_URL')}}" data-port="{{env('SOCKET_PORT')}}">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="data" class="col-md-4 control-label">Data</label>
                        <div class="col-md-6">
                            <input type="number" id="data" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
                <br>
                <hr>
                <br>
                <div id="socket-response">
                    <ul id="socket-response"></ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{URL::asset('js/sockets.js')}}"></script>
@endsection
