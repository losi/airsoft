<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 29.05.2016
 * Time: 9:47
 */

namespace Sockets;


use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Illuminate\Support\Facades\Auth;
use Airsoft\Entities\User;
use Airsoft\Repo\UserRepo\UserEloquentRepo;

class Chat implements MessageComponentInterface
{
    protected $clients;

    /**
     * Chat constructor.
     */
    public function __construct()
    {
        $this->clients = new \SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
//        ob_start();
//        var_dump($conn);
//        $result = ob_get_clean();
//        file_put_contents('socket_test.txt', $result);
//        ob_flush();
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $clientsNum = count($this->clients) - 1;

        echo sprintf('Connection %d sending message %s to %d other connection%s' . "\n",
            $from->resourceId, $msg, $clientsNum, $clientsNum === 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $client->send($msg);
                
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} is closed.\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred {$e->getMessage()}\n";
        $conn->close();
    }
}