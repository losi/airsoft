<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 29.05.2016
 * Time: 9:52
 */

require dirname(__DIR__) . '/vendor/autoload.php';

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Sockets\Chat;

$server = IoServer::factory(new HttpServer(new WsServer(new Chat())), (int)env('SOCKET_PORT'));

$server->run();