<?php
/**
 * Created by PhpStorm.
 * User: losi
 * Date: 29.05.2016
 * Time: 9:52
 */

require dirname(__DIR__).'/vendor/autoload.php';

use Ratchet\Server\IoServer;
use Sockets\Chat;

$server = IoServer::factory(new Chat(), 8000);

$server->run();