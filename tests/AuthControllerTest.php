<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthControllerTest extends TestCase
{
    /**
     * Test registration of new User.
     * @return void
     */
    public function testRegistration()
    {
        User::where('email', 'test@test.com')->forceDelete();

        $response = $this->call('post', '/register', [
            'name'                  => 'test',
            'email_registration'    => 'test@test.com',
            'password'              => '123456',
            'password_confirmation' => '123456',
        ]);
        $this->assertResponseStatus(302);
        $this->assertSessionHas('message');
        $user = User::where('email', 'test@test.com')->firstOrFail();
        $this->assertEquals('test', $user->name);
        $this->login($user);
    }

    public function login(User $user)
    {
        $user->confirmed = 1;
        $user->save();
        $this->call('post', '/login', ['email' => 'test@test.com', 'password_login' => '123456']);
        $this->assertRedirectedTo('/home');
    }
}
