<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Airsoft\Entities\User;

class UserEntityTest extends \PHPUnit_Framework_TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTrowExceptionOnUserCreatedWithoutMandatoryProperties()
    {
        $this->setExpectedException('Exception');
        $user = new User();
    }
    
    public function testUserCreatedWithMandatoryProperties() {
        $userRepo = new \Airsoft\Repo\UserRepo\UserEloquentRepo(new \App\User());
        $user = new User('test', 'test', 'test', 'test', $userRepo);
        $this->assertInstanceOf('Airsoft\Entities\User', $user);
    }
}
